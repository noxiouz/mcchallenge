package locks

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"
)

const lockPattern = ".lock"

// MatchFunc ...
type MatchFunc func(pid uint64, path string) error

type inodet uint64

// Locks contains mapping from a filepatj of explored lock file into its Ino + Dev
type Locks struct {
	locks map[inodet]string
}

func newLocks() *Locks {
	return &Locks{
		locks: make(map[inodet]string),
	}
}

// walkFunc implements filepath.WalkFunc
// It's passed filepath.Walk to explore locks
func (l *Locks) walkFunc(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	// We don't care about dirs as we needn't job name
	if info.IsDir() {
		return nil
	}

	// lockfiles are expected to be matched to lockPattern
	matched := strings.HasSuffix(path, lockPattern)
	if !matched {
		return nil
	}

	statinfo, ok := info.Sys().(*syscall.Stat_t)
	if !ok {
		return fmt.Errorf("Sys() returns unexpected type %T. This platform is not supported", info.Sys())
	}
	l.locks[inodet(statinfo.Ino)] = path
	return nil
}

// WalkLocks matches discovered locks with PIDs
func (l *Locks) WalkLocks(r io.Reader, f MatchFunc) error {
	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		body := scanner.Bytes()
		pid, inode, err := infoProcLocks(body)
		if err != nil {
			return err
		}

		path, ok := l.locks[inodet(inode)]
		if ok {
			if err = f(pid, path); err != nil {
				return err
			}
		}
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func infoProcLocks(body []byte) (pid uint64, inode uint64, err error) {
	const (
		expFieldsLen = 8
		PIDpos       = 4
		InodePos     = 5
	)
	// NOTE: expected format of proc/locks
	// id: T     LOCKTYPE  ACCESS PID md:md:inode st fn
	// 1: POSIX  ADVISORY  WRITE 3568 fd:00:2531452 0 EOF
	// We care about PID and inode
	fields := bytes.Fields(body)
	if len(fields) != expFieldsLen {
		return 0, 0, fmt.Errorf("malformed locks line: length %d is not equal to expected %d", len(fields), expFieldsLen)
	}

	pid, err = strconv.ParseUint(string(fields[PIDpos]), 10, 64)
	if err != nil {
		return 0, 0, fmt.Errorf("malfored locks line: failed to parse pid from %s %v", fields[PIDpos], err)
	}

	// MAJOR-DEVICE:MINOR-DEVICE:INODE-NUMBER
	inodeinfo := bytes.Split(fields[InodePos], []byte(":"))
	if len(inodeinfo) != 3 {
		return 0, 0, fmt.Errorf("malfored locks line: inode must be MAJOR-DEVICE:MINOR-DEVICE:INODE-NUMBER %s", fields[InodePos])
	}

	inode, err = strconv.ParseUint(string(inodeinfo[2]), 10, 64)
	if err != nil {
		return 0, 0, fmt.Errorf("malformed locks line: failed to parse inode from %s %v", inodeinfo[2], err)
	}

	return pid, inode, nil
}

func exploreLocks(root string) (*Locks, error) {
	var locks = newLocks()
	if err := filepath.Walk(root, locks.walkFunc); err != nil {
		return nil, err
	}

	return locks, nil
}

// New creates new Locks with lockfiles discovered under root
func New(root string) (*Locks, error) {
	stat, err := os.Lstat(root)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, fmt.Errorf("given root %s is not exist", root)
		}

		return nil, err
	}

	if !stat.IsDir() {
		return nil, fmt.Errorf("given root %s is not a directory", root)
	}

	return exploreLocks(root)
}
