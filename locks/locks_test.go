package locks

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"syscall"
	"testing"
)

func TestLocks(t *testing.T) {
	dir, err := ioutil.TempDir("", "lslocktest")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir)

	lockdir := filepath.Join(dir, "lock")
	if err = os.MkdirAll(lockdir, 0755); err != nil {
		t.Fatal(err)
	}

	explocks := []string{"a.lock", "b.lock", "c.lock"}
	nonlocks := []string{"d", "e"}
	dirs := []string{"dir1", "dir2"}
	inodes := make(map[uint64]string)
	for _, lock := range explocks {
		p := filepath.Join(lockdir, lock)
		f, err := os.Create(p)
		if err != nil {
			t.Fatal(err)
		}
		fi, err := f.Stat()
		if err != nil {
			t.Fatal(err)
		}
		st, ok := fi.Sys().(*syscall.Stat_t)
		if !ok {
			t.Fatal("unsupported platform")
		}
		inodes[st.Ino] = p
		f.Close()
	}

	for _, nonlock := range nonlocks {
		f, err := os.Create(filepath.Join(lockdir, nonlock))
		if err != nil {
			t.Fatal(err)
		}
		f.Close()
	}
	for _, fdir := range dirs {
		if err = os.MkdirAll(filepath.Join(dir, fdir), 0755); err != nil {
			t.Fatal(err)
		}
	}

	locks, err := New(dir)
	if err != nil {
		t.Fatal(err)
	}
	if len(locks.locks) != len(inodes) {
		t.Fatalf("%v %v mismathced lengths", len(locks.locks), len(inodes))
	}
	for k, v := range inodes {
		if lv := locks.locks[inodet(k)]; lv != v {
			t.Fatalf("value with %d mismathed: %v %v", k, lv, v)
		}
	}

	var buff = new(bytes.Buffer)
	var pids = make(map[uint64]string)
	for in, path := range inodes {
		rndpid := rand.Uint64()
		pids[rndpid] = path
		fmt.Fprintf(buff, "1: POSIX  ADVISORY  WRITE %d fd:00:%d 0 EOF\n", rndpid, in)
	}
	var found int
	err = locks.WalkLocks(buff, func(pid uint64, path string) error {
		p, ok := pids[pid]
		if !ok {
			t.Fatal("unknown pid ", pid)
		}
		if p != path {
			t.Fatal("unknown path ", path)
		}
		found++
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}

	if len(pids) != found {
		t.Fatal("did not found all pids", found, pids)
	}
}
