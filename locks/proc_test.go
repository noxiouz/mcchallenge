package locks

import (
	"errors"
	"testing"
)

func TestParseProcLockFile(t *testing.T) {
	fixtures := []struct {
		Name  string
		Body  string
		Pid   uint64
		Inode uint64
		Err   error
	}{
		{"empty case", "", 0, 0, errors.New("malformed locks line: length 0 is not equal to expected 8")},
		{"malformed PID", "1: POSIX  ADVISORY  WRITE AAAA fd:00:2531452 0 EOF", 0, 0,
			errors.New(`malfored locks line: failed to parse pid from AAAA strconv.ParseUint: parsing "AAAA": invalid syntax`)},
		{"malformed inode", "1: POSIX  ADVISORY  WRITE 3568 fd:00:XXX 0 EOF", 0, 0,
			errors.New(`malformed locks line: failed to parse inode from XXX strconv.ParseUint: parsing "XXX": invalid syntax`),
		},
		{"linux proc", "1: POSIX  ADVISORY  WRITE 3568 fd:00:2531452 0 EOF", 3568, 2531452, nil},
	}

	for _, fixt := range fixtures {
		pid, inode, err := infoProcLocks([]byte(fixt.Body))
		if (err != nil) != (fixt.Err != nil) {
			t.Fatalf("%s: error `%v` is not equal to fixture `%v`\n", fixt.Name, err, fixt.Err)
		}
		if err != nil && err.Error() != fixt.Err.Error() {
			t.Fatalf("%s: error `%v` is not equal to fixture `%v`\n", fixt.Name, err, fixt.Err)
		}
		if pid != fixt.Pid {
			t.Fatalf("%s: PID %d is not equal to fixture %d\n", fixt.Name, pid, fixt.Pid)
		}
		if inode != fixt.Inode {
			t.Fatalf("%s: Inode %d is not equal to fixture %d\n", fixt.Name, inode, fixt.Inode)
		}
	}
}
