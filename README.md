lslocks
=======

Build requirements
`go 1.8`

Limitation
==========

I do not use major and minor device numbers to recognize files.
Only Inode is taken into account.

How to Build
============

This command builds both `lslock` and `lslock-test` files

```make build```

How to Run
==========

```lslock -dir <path>```


How to test
===========

+ package tests: ```make test```
+ test via *lslock-test*: ```make lslock-test```


lslock-test
===========

`lslock-test` expects `lslock` to be in $PATH
