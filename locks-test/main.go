package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"syscall"
	"time"
)

var (
	willFork = flag.Bool("fork", false, "will fork")
	path     = flag.String("lockpath", "", "lock path")
)

func flock(path string) error {
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return err
	}
	defer f.Close()

	if err = syscall.Flock(int(f.Fd()), syscall.LOCK_EX); err != nil {
		return err
	}
	time.Sleep(time.Second * 30)
	return nil
}

func main() {
	flag.Parse()
	if *willFork {
		if err := flock(*path); err != nil {
			log.Fatal(err)
		}
		return
	}

	if err := os.MkdirAll("/tmp/lslock-test", 0755); err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll("/tmp/lslock-test")

	forks := make(map[int]string)
	children := []string{"A", "B", "C"}
	for _, child := range children {
		path := fmt.Sprintf("/tmp/lslock-test/lock%s/%s.lock", child, child)
		os.MkdirAll(filepath.Dir(path), 0777)
		fork := exec.Command(os.Args[0], "-fork", "-lockpath", path)
		if err := fork.Start(); err != nil {
			log.Fatal(err)
		}
		defer fork.Process.Kill()

		fmt.Printf("forked a process PID %d PATH %s\n", fork.Process.Pid, path)
		forks[fork.Process.Pid] = path
	}

	out, err := exec.Command("lslock", "-dir", "/tmp/lslock-test").Output()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("==========================")
	fmt.Printf("lslock output:\n%s\n", out)

	scanner := bufio.NewScanner(bytes.NewReader(out))
	for scanner.Scan() {
		var pid uint64
		var pth string
		var n int
		n, err = fmt.Sscanf(scanner.Text(), "%d %s", &pid, &pth)
		if err != nil {
			log.Fatal(err)
		}
		if n != 2 {
			continue
		}

		p, ok := forks[int(pid)]
		if !ok {
			log.Fatalf("unknown pid %d", pid)
		}
		if p != pth {
			log.Fatalf("unknown path %s", pth)
		}
		fmt.Printf("lslock detects %d %s\n", pid, pth)
		delete(forks, int(pid))
	}
	if scanner.Err() != nil {
		log.Fatal(err)
	}
	if len(forks) != 0 {
		log.Fatalf("some locks were not detected %v", forks)
	}
}
