

build:
	go build -o lslock ./cmd/main.go
	go build -o lslock-test ./locks-test/main.go

test:
	go test -v ./locks

lslock-test: build
	PATH=${PATH}:$(pwd) go run ./locks-test/main.go
