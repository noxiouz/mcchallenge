package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"bitbucket.org/noxiouz/mcchallenge/locks"
)

const proclockspath = "/proc/locks"

var (
	dir = flag.String("dir", "", " a directory to looking for lock files")
)

func main() {
	flag.Parse()
	if *dir == "" {
		log.Fatal("dir argument must be non-empty")
	}
	locks, err := locks.New(*dir)
	if err != nil {
		log.Fatalf("%v", err)
	}

	locksfile, err := os.Open(proclockspath)
	if err != nil {
		log.Fatalf("%v", err)
	}
	defer locksfile.Close()

	printOnMatch := func(pid uint64, path string) error {
		fmt.Printf("%d %s\n", pid, path)
		return nil
	}
	if err = locks.WalkLocks(locksfile, printOnMatch); err != nil {
		log.Fatalf("%v", err)
	}
}
